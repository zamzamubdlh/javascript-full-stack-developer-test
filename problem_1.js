//Javascript program to find the only repeating

// element in an array where elements are
// from 1 to N-1.
function findRepeating(arr, N){
    arr.sort();
    for (let i = 0; i < N; i++) {
        // compare array element with its index
        if (arr[i] != i + 1) {
            return arr[i];
        }
    }
    return -1;
}
           // Driver code
let arr= [ 1, 2, 3, 4, 4 ];
let N = arr.length;

// Function call
console.log(findRepeating(arr, N));
