// javascript program to print all combination of size r in an array

// of size n with repetitions allowed
/* arr ---> Input Array chosen ---> Temporary array to store indices of current combination start & end ---> Starting and Ending indexes in arr ---> Size of a combination to be printed */

function CombinationRepetitionUtil(chosen , arr,
    index , r , start , end)
{

// Since index has become r, current combination is
// ready to be printed, print
if (index == r) {
    for (var i = 0; i < r; i++) {
        document.write(arr[chosen[i]]);
    }
    return;
}

// One by one choose all elements (without considering
// the fact whether element is already chosen or not)
// and recur
for (var i = start; i <= end; i++) {
    chosen[index] = i;
    CombinationRepetitionUtil(chosen, arr, index + 1, r, i, end);
}
return;
}

// The main function that prints all combinations of size r
// in arr of size n with repetitions. This function mainly
// uses CombinationRepetitionUtil()
function CombinationRepetition(arr , n , r)
{

// Allocate memory
var chosen = Array.from({length: (r + 1)}, (_, i) => 0);

// Call the recursive function
CombinationRepetitionUtil(chosen, arr, 0, r, 0, n - 1);
}

// Driver program to test above functions
var arr = [1, 2, 3];
var n = arr.length;
var r = 2;
CombinationRepetition(arr, n, r);
