// Javascript program to find Maximum Product Subarray

/* Returns the product of max product subarray.*/
function maxSubarrayProduct(arr, n)
{
    // Initializing result
    let result = arr[0];

    for (let i = 0; i < n; i++)
    {
        let mul = arr[i];
        // traversing in current subarray
        for (let j = i + 1; j < n; j++)
        {
            // updating result every time
            // to keep an eye over the maximum product
            result = Math.max(result, mul);
            mul *= arr[j];
        }
        // updating the result for (n-1)th index.
        result = Math.max(result, mul);
    }
    return result;
}

// Driver code
let arr = [ -6, 4, -5, 8, -10, 0, 8 ];
let n = arr.length;
document.write("The maximum product sub-array is [4,-5,8,-10] having product " + maxSubarrayProduct(arr, n));
